from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json
import requests


# code here to make http requests to Pexels and OWM.
# return Python dictionaries to be used by the view code.

# Code from lecture shows how to use the requests module, use an ACL function,
# and integrate that into a view.


def get_location_pictures():
    pass


def get_location_weather():
    pass
